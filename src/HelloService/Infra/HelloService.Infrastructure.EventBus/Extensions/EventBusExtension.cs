﻿using HelloService.Core.Interface;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloService.Infrastructure.EventBus.Extensions
{
    public static class EventBusExtension
    {
        public static IServiceCollection AddRabbitMQEventBus(this IServiceCollection services)
        {
            services.AddSingleton<IEventBus, RabbitMQEventBus>(provider => {
                var scopeFactory = provider.GetRequiredService<IServiceScopeFactory>();
                var logger = provider.GetRequiredService<ILogger<RabbitMQEventBus>>();
                var mediator = provider.GetRequiredService<IMediator>();
                return new RabbitMQEventBus(mediator, scopeFactory, logger);
            });
            return services;
        }
    }
}
