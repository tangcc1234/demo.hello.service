﻿using DistributedCache.Domain;
using Microsoft.AspNetCore.Mvc;
using DistributedCache.Application.Services;

namespace DistributedCache.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HelloController : Controller
    {
        private readonly IHelloService _helloService;
        private readonly ILogger<HelloController> _logger;

        public HelloController(IHelloService helloService, ILogger<HelloController> logger)
        {
            _helloService = helloService;
            _logger = logger;
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<string>> Greet(string userId)
        {
            var greeting = await _helloService.Greeting(userId);

            if (string.IsNullOrEmpty(greeting))
            {
                return NotFound();
            }

            return Ok(greeting);
        }
    }
}
