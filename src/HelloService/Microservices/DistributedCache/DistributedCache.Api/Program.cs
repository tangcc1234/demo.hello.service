using DistributedCache.Application.Services;
using DistributedCache.Domain;
using DistributedCache.Domain.EventHandlers;
using DistributedCache.Domain.Events;
using DistributedCache.Infrastructure;
using HelloService.Core.EventBus;
using HelloService.Core.Interface;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using HelloService.Infrastructure.EventBus.Extensions;
using System.Reflection;
using MediatR;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
var configuration = builder.Configuration;
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = builder.Configuration.GetValue<string>("Redis:Connection");
});
builder.Services.Add(ServiceDescriptor.Singleton<IDistributedCache, RedisCache>());
builder.Services.AddHealthChecks();
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());
builder.Services.AddScoped(typeof(IDistributedCacheRepo<>), typeof(DistributedCacheRepo<>));
builder.Services.AddScoped<IHelloService, DistributedCache.Application.Services.HelloService>();
builder.Services.AddHttpClient<IUsersService, DistributedCache.Application.Services.UsersService>((provider, client) =>
{
    client.BaseAddress = new Uri(provider.GetService<IConfiguration>().GetValue<string>("UsersService"));
});

builder.Services.AddTransient<CreateUserEventHandler>();
builder.Services.AddTransient<UpdateUserEventHandler>();
builder.Services.AddTransient<DeleteUserEventHandler>();

builder.Services.AddTransient<IEventHandler<UserCreatedEvent>, CreateUserEventHandler>();
builder.Services.AddTransient<IEventHandler<UserUpdatedEvent>, UpdateUserEventHandler>();
builder.Services.AddTransient<IEventHandler<UserDeletedEvent>, DeleteUserEventHandler>();

builder.Services.AddRabbitMQEventBus();

var app = builder.Build();



app.MapHealthChecks("/hs", new HealthCheckOptions
{
    AllowCachingResponses = false,
    ResultStatusCodes =
    {
        [HealthStatus.Healthy] = StatusCodes.Status200OK,
        [HealthStatus.Degraded] = StatusCodes.Status200OK,
        [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
    },
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

var eventBus = app.Services.GetRequiredService<IEventBus>();
var connectionString = builder.Configuration.GetValue<string>("RabbitMqConnectionString");
eventBus.Subscribe<UserCreatedEvent, CreateUserEventHandler>(connectionString);
eventBus.Subscribe<UserUpdatedEvent, UpdateUserEventHandler>(connectionString);
eventBus.Subscribe<UserDeletedEvent, DeleteUserEventHandler>(connectionString);


app.Run();
