﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCache.Application.Services
{
    public interface IHelloService
    {
        Task<string> Greeting(string userId);
    }
}
