﻿using DistributedCache.Domain;
using DistributedCache.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCache.Application.Services
{
    public class HelloService : IHelloService
    {
        private readonly IDistributedCacheRepo<User> _cachedRepo;
        private readonly IUsersService _usersService;
        public HelloService(IDistributedCacheRepo<User> cachedRepo, IUsersService usersService)
        {
            _cachedRepo = cachedRepo;
            _usersService = usersService;
        }


        public async Task<User> GetUser(string userId)
        {
            return await _cachedRepo.Get($"{ServiceNames.User}-{userId}");
        }
        public async Task<string> Greeting(string userId)
        {
            var user = await this.GetUser(userId);
            var defaultGreeting = "Hello, World!";
            if(user == null)
            {
                user = await this._usersService.Get(userId);
                if(user == null)
                {
                    return defaultGreeting;
                }
                else
                {
                    await _cachedRepo.Update($"{ServiceNames.User}-{userId}", user);
                }
            }
            return $"Hello, {user.Name}.";
        }
    }
}
