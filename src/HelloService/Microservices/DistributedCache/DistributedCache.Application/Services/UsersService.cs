﻿using DistributedCache.Domain;
using DistributedCache.Domain.Constants;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCache.Application.Services
{
    public class UsersService : IUsersService
    {
        public const string ErrorMessageTemplate =
            "Call UsersService {MethodName}, Status Code: {StatusCode}, Request Url: {url}";
        private readonly HttpClient _httpClient;

        private readonly ILogger<UsersService> _logger;
        public UsersService(HttpClient client, ILogger<UsersService> logger)
        {
            _httpClient = client;
            _logger = logger;
        }

        public async Task<User?> Get(string key)
        {
            var requestUrl = $"api/Users/{key}";
            try
            {
                var response = await this._httpClient.GetAsync(requestUrl);
                if (!response.IsSuccessStatusCode)
                {
                    this._logger.LogError(
                        ErrorMessageTemplate,
                        "Get Users ",
                        response.StatusCode,
                        requestUrl);

                    return null;
                }

                var ret = JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
                return ret;
            }
            catch (Exception e)
            {
                this._logger.LogError(
                    e,
                    "Unhandled {ExceptionType} in Hello Service for requesturl = {RequestUrl}",
                    e.GetType().Name,
                    requestUrl);
            }

            return null;
        }
    }
}
