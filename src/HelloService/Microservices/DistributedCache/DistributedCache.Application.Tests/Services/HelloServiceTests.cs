﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DistributedCache.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistributedCache.Domain;
using Moq;
using DistributedCache.Domain.Constants;

namespace DistributedCache.Application.Services.Tests
{
    [TestClass()]
    public class HelloServiceTests
    {
        [TestMethod()]
        public async Task GreetingTest_GetFromCache()
        {
            //arrange
            var mockUserService = new Mock<IUsersService>();
            var mockCacheRepo = new Mock<IDistributedCacheRepo<User>>();
            var expectedUserName = "Bob";
            var userId = "123";
            var expectedUser = new User { Id = userId, Name= expectedUserName };
            var cacheKey = $"{ServiceNames.User}-{userId}";
            mockCacheRepo.Setup(repo => repo.Get(cacheKey)).ReturnsAsync(expectedUser);
            mockUserService.Setup(s=>s.Get(userId)).ReturnsAsync(expectedUser);

            var expected = $"Hello, {expectedUserName}.";
            var helloService = new HelloService(mockCacheRepo.Object, mockUserService.Object);

            //act
            
            var result  = await helloService.Greeting(userId);
            //assert
            Assert.IsTrue(string.Equals(expected, result));
        }

        [TestMethod()]
        public async Task GreetingTest_GetDefault()
        {
            //arrange
            var mockUserService = new Mock<IUsersService>();
            var mockCacheRepo = new Mock<IDistributedCacheRepo<User>>();
            var expectedUserName = "Bob";
            var userId = "123";
            var expectedUser = new User { Id = userId, Name = expectedUserName };
            var cacheKey = $"{ServiceNames.User}-{userId}";
            mockCacheRepo.Setup(repo => repo.Get(cacheKey)).Returns(Task.FromResult<User>(null));
            mockUserService.Setup(s => s.Get(userId)).Returns(Task.FromResult<User>(null));

            var expected = $"Hello, World!";
            var helloService = new HelloService(mockCacheRepo.Object, mockUserService.Object);

            //act

            var result = await helloService.Greeting(userId);
            //assert
            Assert.IsTrue(string.Equals(expected, result));
        }
    }
}