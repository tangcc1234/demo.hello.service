﻿using DistributedCache.Domain;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace DistributedCache.Infrastructure
{
    public class DistributedCacheRepo<T> : IDistributedCacheRepo<T>
    {
        private const int ABSOLUTE_EXP_DAY = 1;
        private const double SLIDING_EXP_DAY = 0.8;
        private readonly IDistributedCache _distributedcache;
        public DistributedCacheRepo(IDistributedCache cache)
        {
            _distributedcache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        public async Task Delete(string key)
        {
            await _distributedcache.RemoveAsync(key);
        }

        public async Task<T> Get(string key)
        {
            var cachedObject = await _distributedcache.GetStringAsync(key);
            return (string.IsNullOrEmpty(cachedObject)) ? default : JsonConvert.DeserializeObject<T>(cachedObject);
        }

        public async Task<T> Update(string key, T value)
        {
            var options = new DistributedCacheEntryOptions()
            .SetAbsoluteExpiration(DateTimeOffset.UtcNow.AddDays(ABSOLUTE_EXP_DAY))
            .SetSlidingExpiration(TimeSpan.FromDays(SLIDING_EXP_DAY));

            var cacheObject = JsonConvert.SerializeObject(value);
            await _distributedcache.SetStringAsync(key, cacheObject, options);
            return await Get(key);
        }
    }
}