﻿using DistributedCache.Domain.Constants;
using DistributedCache.Domain.Events;
using HelloService.Core.EventBus;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DistributedCache.Domain.EventHandlers
{
    public class UpdateUserEventHandler : IEventHandler<UserUpdatedEvent>
    {
        private readonly IDistributedCacheRepo<User> _repository;
        private readonly ILogger<UpdateUserEventHandler> _logger;

        public UpdateUserEventHandler(IDistributedCacheRepo<User> repository, ILogger<UpdateUserEventHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task Handle(UserUpdatedEvent @event)
        {

            var user = @event.user;
            if (string.IsNullOrEmpty(user.Id))
            {
                this._logger.LogError($"User Update Event user ID is null. {user.Name}");
                return;
            }
            await _repository.Update(
            $"{ServiceNames.User}-{user.Id.ToString()}",
            new User()
            {
                Id = @event.user.Id,
                Name = @event.user.Name
            });

        }
    }
}
