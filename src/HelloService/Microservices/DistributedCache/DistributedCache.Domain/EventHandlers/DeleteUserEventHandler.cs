﻿using DistributedCache.Domain.Constants;
using DistributedCache.Domain.Events;
using HelloService.Core.EventBus;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DistributedCache.Domain.EventHandlers
{
    public class DeleteUserEventHandler : IEventHandler<UserDeletedEvent>
    {
        private readonly IDistributedCacheRepo<User> _repository;
        private readonly ILogger<DeleteUserEventHandler> _logger;

        public DeleteUserEventHandler(IDistributedCacheRepo<User> repository, ILogger<DeleteUserEventHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task Handle(UserDeletedEvent @event)
        {
            var user = @event.user;
            if (string.IsNullOrEmpty(user.Id))
            {
                this._logger.LogError($"User Delete Event user ID is null. {user.Name}");
                return;
            }
            await _repository.Delete(
            $"{ServiceNames.User}-{user.Id.ToString()}");

        }
    }
}
