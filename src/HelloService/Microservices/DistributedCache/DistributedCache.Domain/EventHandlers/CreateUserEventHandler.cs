﻿using DistributedCache.Domain.Constants;
using DistributedCache.Domain.Events;
using HelloService.Core.EventBus;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DistributedCache.Domain.EventHandlers
{
    public class CreateUserEventHandler : IEventHandler<UserCreatedEvent>
    {
        private readonly IDistributedCacheRepo<User> _repository;
        private readonly ILogger<CreateUserEventHandler> _logger;

        public CreateUserEventHandler(IDistributedCacheRepo<User> repository, ILogger<CreateUserEventHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task Handle(UserCreatedEvent @event)
        {
            var user = @event.user;
            if(string.IsNullOrEmpty(user.Id))
            {
                this._logger.LogError($"User Create Event user ID is null. {user.Name} Create Cache failure");
                return;
            }
            await _repository.Update(
            $"{ServiceNames.User}-{user.Id.ToString()}",
            new User()
            {
                Id = @event.user.Id,
                Name = @event.user.Name
            });

        }
    }
}
