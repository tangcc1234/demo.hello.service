﻿namespace DistributedCache.Domain
{
    public interface IDistributedCacheRepo<T>
    {
        Task<T> Get(string key);
        Task<T> Update(string key, T cacheObject);
        Task Delete(string key);

    }
}