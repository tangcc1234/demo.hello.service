﻿using HelloService.Core.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCache.Domain.Events
{
    public class UserUpdatedEvent : Event
    {
        public User user { get; set; }
    }
}
