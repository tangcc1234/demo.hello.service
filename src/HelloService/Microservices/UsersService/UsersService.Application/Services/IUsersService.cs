﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersService.Domain.Models;

namespace UsersService.Application.Services
{
    public interface IUsersService
    {
        Task<List<User>> GetAsync();

        Task<User?> GetAsync(string id);

        Task CreateAsync(User user);

        Task UpdateAsync(string id, User user);

        Task RemoveAsync(string id);
    }
}
