﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloService.Core.Interface;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using UsersService.Application.Commands;
using UsersService.Domain.Models;

namespace UsersService.Application.Services
{
    public class UsersService : IUsersService
    {

        private readonly IMongoCollection<User> _usersCollection;
        private readonly IEventBus _bus;
        private readonly ILogger<UsersService> _logger;
        public UsersService(IMongoClient mongoClient,
            IOptions<UserProfilesDBSettings> userProfilesDBSettings, IEventBus bus, ILogger<UsersService> logger)
        {
            var mongoDatabase = mongoClient.GetDatabase(
                userProfilesDBSettings.Value.DatabaseName);

            _usersCollection = mongoDatabase.GetCollection<User>(
                userProfilesDBSettings.Value.UsersCollectionName);
            _logger = logger;
            _bus = bus;
        }

        public async Task<List<User>> GetAsync() =>
            await _usersCollection.Find(_ => true).ToListAsync();

        public async Task<User?> GetAsync(string id) =>
            await _usersCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task CreateAsync(User user)
        {
            await _usersCollection.InsertOneAsync(user);

            var command = new CreateUserCommand(user);

            await _bus.SendCommand(command);
        }

        public async Task UpdateAsync(string id, User user)
        {

            await _usersCollection.ReplaceOneAsync(x => x.Id == id, user);
            var command = new UpdateUserCommand(user);

            await _bus.SendCommand(command);
        }

        public async Task RemoveAsync(string id)
        {
            await _usersCollection.DeleteOneAsync(x => x.Id == id);

            var command = new DeleteUserCommand(new User { Id = id });

            await _bus.SendCommand(command);
        }
    }
}
