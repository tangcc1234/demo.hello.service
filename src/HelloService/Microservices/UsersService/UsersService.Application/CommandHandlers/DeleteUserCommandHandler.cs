﻿using HelloService.Core.Interface;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersService.Application.Commands;
using UsersService.Domain.Events;

namespace UsersService.Application.CommandHandlers
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly IEventBus _bus;
        private readonly IConfiguration _configuration;

        public DeleteUserCommandHandler(IEventBus bus, IConfiguration configuration)
        {
            _bus = bus;
            _configuration = configuration;
        }

        public Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            // publish event to RabbitMq
            _bus.Publish(new UserDeletedEvent(request.user), _configuration.GetConnectionString("RabbitMqConnectionString"));
            return Task.FromResult(true);
        }
    }
}
