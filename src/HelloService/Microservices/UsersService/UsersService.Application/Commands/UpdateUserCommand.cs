﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersService.Domain.Models;

namespace UsersService.Application.Commands
{
    public class UpdateUserCommand : UserCommand
    {
        public UpdateUserCommand(User user)
        {
            this.user = user;
        }
    }
}
