﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersService.Domain.Models;

namespace UsersService.Application.Commands
{
    public class CreateUserCommand : UserCommand
    {
        public CreateUserCommand(User user)
        {
            this.user = user;
        }
    }
}
