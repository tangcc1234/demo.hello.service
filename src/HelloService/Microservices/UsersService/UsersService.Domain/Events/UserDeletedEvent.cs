﻿using HelloService.Core.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersService.Domain.Models;

namespace UsersService.Domain.Events
{
    public class UserDeletedEvent : Event
    {
        public User user { get; private set; }

        public UserDeletedEvent(User user)
        {
            this.user = user;
        }
    }
}
