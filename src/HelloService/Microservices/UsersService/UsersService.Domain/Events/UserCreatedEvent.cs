﻿using HelloService.Core.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersService.Domain.Models;

namespace UsersService.Domain.Events
{
    public class UserCreatedEvent : Event
    {
        public User user { get; private set; }
        public UserCreatedEvent(User user)
        {
            this.user = user;
        }
    }
}
