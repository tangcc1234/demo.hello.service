using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using UsersService.Domain.Models;
using UsersService.Application.Services;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using System.Reflection;

using MediatR;
using HelloService.Infrastructure.EventBus.Extensions;
using UsersService.Application.Commands;
using UsersService.Application.CommandHandlers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHealthChecks();
builder.Services.Configure<UserProfilesDBSettings>(
    builder.Configuration.GetSection("UserProfilesDatabase"));

builder.Services.AddSingleton<IMongoClient>(sp =>
{
    var settings = sp.GetService<IOptions<UserProfilesDBSettings>>();
    var client = new MongoClient(settings.Value.ConnectionString);
    return client;

});
builder.Services.AddTransient<IUsersService, UsersService.Application.Services.UsersService>();
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());
builder.Services.AddTransient<IRequestHandler<CreateUserCommand, bool>, CreateUserCommandHandler>();
builder.Services.AddTransient<IRequestHandler<DeleteUserCommand, bool>, DeleteUserCommandHandler>();
builder.Services.AddTransient<IRequestHandler<UpdateUserCommand, bool>, UpdateUserCommandHandler>();

builder.Services.AddRabbitMQEventBus();
var app = builder.Build();


app.MapHealthChecks("/hs", new HealthCheckOptions
{
    AllowCachingResponses = false,
    ResultStatusCodes =
    {
        [HealthStatus.Healthy] = StatusCodes.Status200OK,
        [HealthStatus.Degraded] = StatusCodes.Status200OK,
        [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
    },
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.Run();
