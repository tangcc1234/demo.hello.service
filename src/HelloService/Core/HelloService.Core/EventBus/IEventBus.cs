﻿using HelloService.Core.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloService.Core.Interface
{
    public interface IEventBus
    {
        /// <summary>
        /// Send the create update delete command for CQRS
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        Task SendCommand<T>(T command) where T : Command;
        void Publish<T>(T @event, string connectionString) where T : Event;
        void Subscribe<T, TH>(string connectionString) where T : Event
                                where TH : IEventHandler<T>;
    }
}
