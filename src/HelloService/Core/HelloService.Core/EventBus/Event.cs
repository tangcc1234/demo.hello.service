﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloService.Core.EventBus
{
    public class Event
    {
        protected Event()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
        }

        public Guid Id { get; private init; }
        public DateTime CreationDate { get; private init; }
    }
}
