﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloService.Core.EventBus
{
    public class Command : IRequest<bool>
    {
        public string MessageType { get; private init; }
        public DateTime CreateDate { get; private init; }
        protected Command()
        {
            CreateDate = DateTime.UtcNow;
            MessageType = GetType().Name;
        }
    }
}
