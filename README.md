# Demo for Hello Service
The project mainly composite by 2 servies:
| Name | Details |
| ------ | ------ |
| Users Service       | This service contain basic CRUD function for User profile       |
| Distributed Cache Service       | This service manage the distributed cache read / write operation. And also act as the Hello Service entry point.      |

## Basic Project Architecture
| Name | Details |
| ------ | ------ |
| Core | The base object or interface used by above 2 services |
| Infra | The shared Event Bus Implmemtation of RabbitMQEventBus. And also act as the Command dispatcher while create / delete / update command is trigger |
| Microservices | This folder contain 2 services file. |

Each microservices may contain following folder structures. <br/>
Remark: my initial objective is applying the clean architecture stated at dotnetconf 2022.<br/>
[Clean Architecture with ASP.NET Core 7 | .NET Conf 2022](https://www.youtube.com/watch?v=j6u7Pw6dyUw) <br/>
But it seems that i mix up with this and N-tier <br/>
### 1. Domain Layer
Domain Layer contain the model or interface
### 2. Application Layer
Application Layer contain the business logic and service logic

### 3. Infrastructure Layer
Infrastructure Layer conatin the DB Access / DB Context 
### 4. Web API Layer 
Web API Layer is just like the presentation layer in N-tier

## Users Service
The service contains the CRUD function connecting to Mongo DB .<br/>
It also use MeditaR and RabbitMq to dispatch the event to Distributed Cache Servive to update the Distributed Cache DB so as to achieve the CQRS pattern in normal operation<br/>

Remark: it would be better setup the mongodb and rabbitmq server before start.<br/>
You may change the config in appsettings.Developement.json if neccssary<br/>

```
  "UserProfilesDatabase": {
    "ConnectionString": "mongodb://localhost:27017",
    "DatabaseName": "UserProfiles",
    "UsersCollectionName": "Users"
  },
  "ConnectionStrings": {

    "RabbitMqConnectionString": "amqp://guest:guest@localhost:5672"
  }
```

You can start this service locally by cmd command<br/>
```
cd ${project-path}\src\HelloService\Microservices\UsersService\UsersService.WebApi
dotnet run
```
you will access the service at port 7064<br/>
swagger entry point at https://localhost:7064/swagger/index.html<br/>


## Distributed Cache Service
This service mainly deal with the cache read / write operation.<br/>
Also its api act as the hello service entry point.<br/>
This service subscribe the event published by UsersService and then update the cache in Redis.<br/>
If the cache is missed, then the distributed cache service would ask the UsersService for the user profile.<br/>

Remark: it would be better setup the RabbitMQ and the Redis before start.<br/>
You may change the config in appsettings.Developement.json if neccssary.
```

  "RabbitMqConnectionString": "amqp://guest:guest@localhost:5672",
  "Redis": {
    "Connection": "localhost:6379,abortConnect=False"
  },
  "UsersService": "http://localhost:5064"
```


You can start this service locally by cmd command<br/>
```
cd ${project-path}\src\HelloService\Microservices\DistributedCache\DistributedCache.Api
dotnet run
```
You will access the service at port 7258<br/>
swagger entry point at https://localhost:7258/swagger/index.html


### Setup Hello Service

#### Set up MongoDB
1. Setup mongodb in local [MongoDB setup in docker](https://hub.docker.com/_/mongo)
2. Connect to mongodb using mongosh through cmd
```
mongosh
```
3. Initialize with new collection('Users')
```
use UserProfiles

//db.Users.drop();
db.Users.find().pretty()

db.Users.insertMany([
{ "name": "Harries"}, 
{ "name": "Bob"}
])

```
#### Set up RabbitMQ
1. Setup rabbitMQ in local [RabbitMQ setup in docker](https://hub.docker.com/_/rabbitmq)
2. Access the RabbitMQ web console at http://localhost:15672/
3. Default using guest account to login RabbitMQ

#### Set up Redis
1. Setup Redis in local [Redis setup in docker](https://hub.docker.com/_/redis)






